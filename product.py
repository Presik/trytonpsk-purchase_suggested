# This file is part of purchase_editable_line module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import date, timedelta
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.model import fields, ModelView


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        if default.get('order_points'):
            default['order_points'] = None
        return super(Template, cls).copy(records, default=default)


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        if default.get('order_points'):
            default['order_points'] = None
        return super(Product, cls).copy(records, default=default)

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()


class UpdateProductBySupplier(Wizard):
    'Update Product By Supplier'
    __name__ = 'purchase_suggested.update_product_supplier'
    start_state = 'update'
    update = StateTransition()

    def transition_update(self):
        pool = Pool()
        ProductSupplier = Pool().get('purchase.product_supplier')
        Purchase = pool.get('purchase.purchase')
        transaction = Transaction()
        cursor = transaction.connection.cursor()
        product_sp = ProductSupplier.__table__()

        cursor.execute(*product_sp.select(
            product_sp.template, product_sp.party))
        products_supp = cursor.fetchall()

        today = date.today()
        start_date = today - timedelta(days=365)

        purchases = Purchase.search([
            ('purchase_date', '>=', start_date),
        ])

        products_purchased = []
        for purchase in purchases:
            party_id = purchase.party.id
            for line in purchase.lines:
                if line.type == 'line' and line.product and line.product.template:
                    if tuple([line.product.template.id, party_id]) not in products_supp \
                    and line.product.template.active:
                        products_purchased.append((line.product.template.id, party_id))
        products_to_create = set(products_purchased)
        # for i, j in products_to_create:
        #     try:
        #         ProductSupplier.create(
        #             [{'template': i, 'party': j}])
        #     except Exception as e:
        #         print(i, j, 'error', e)
        ProductSupplier.create(
            [{'template': i, 'party': j} for i, j in products_to_create]
        )
        return 'end'


class AddSupplierToProductsStart(ModelView):
    'Add Supplier To Products Start'
    __name__ = 'purchase_suggested.add_supplier.start'
    supplier = fields.Many2One('party.party', 'Supplier', required=True)


class AddSupplierToProducts(Wizard):
    'Add Supplier To Products'
    __name__ = 'purchase_suggested.add_supplier'
    start = StateView('purchase_suggested.add_supplier.start',
        'purchase_suggested.add_supplier_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        ids = Transaction().context['active_ids']
        ProductSupplier = Pool().get('purchase.product_supplier')
        suppliers_to_add = []
        for product_id in ids:
            ps = ProductSupplier.search([
                ('party', '=', self.start.supplier.id),
                ('product', '=', product_id),
            ])
            if ps:
                continue
            suppliers_to_add.append({
                'party': self.start.supplier.id,
                'product': product_id,
            })
        ProductSupplier.create(suppliers_to_add)
        return 'end'


class UpdateCostPriceStart(ModelView):
    'Update Cost Price Start'
    __name__ = 'purchase_suggested.update_cost_price.start'
    cost_price = fields.Numeric('New Cost Price', digits=(16, 4),
        required=True)


class UpdateCostPrice(Wizard):
    'Update Cost Price'
    __name__ = 'purchase_suggested.update_cost_price'
    start = StateView('purchase_suggested.update_cost_price.start',
        'purchase_suggested.update_cost_price_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        id = Transaction().context['active_id']
        Template = Pool().get('product.template')
        template = Template(id)
        for product in template.products:
            product.write([product], {'cost_price': self.start.cost_price})
        return 'end'
